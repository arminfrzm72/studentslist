﻿using StudentsList.Data.Entities.Common;

namespace StudentsList.Data.Repository;

public interface IGenericRepository<TEntity> : IDisposable where TEntity : BaseEntity
{
    IQueryable<TEntity> GetEntitiesQuery();

    Task<TEntity?> GetEntityById(Guid entityId);

    Task AddEntity(TEntity entity);

    void UpdateEntity(TEntity entity);

    void RemoveEntity(TEntity entity);

    Task RemoveEntity(Guid entityId);

    Task SaveChanges();
}