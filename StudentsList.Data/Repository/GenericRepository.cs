﻿using Microsoft.EntityFrameworkCore;
using StudentsList.Data.Context;
using StudentsList.Data.Entities.Common;

namespace StudentsList.Data.Repository;

public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseEntity
{
    #region constructor

    private readonly StudentsListContext _context;
    private readonly DbSet<TEntity> _dbSet;

    public GenericRepository(StudentsListContext context)
    {
        _context = context;
        _dbSet = _context.Set<TEntity>();
    }

    #endregion

    public IQueryable<TEntity> GetEntitiesQuery()
    {
        return _dbSet.AsQueryable();
    }

    public async Task<TEntity?> GetEntityById(Guid entityId)
    {
        return await _dbSet.SingleOrDefaultAsync(e => e.Id == entityId);
    }

    public async Task AddEntity(TEntity entity)
    {
        await _dbSet.AddAsync(entity);
    }

    public void UpdateEntity(TEntity entity)
    {
        _dbSet.Update(entity);
    }

    public void RemoveEntity(TEntity entity)
    {
        entity.IsDeleted = true;
        UpdateEntity(entity);
    }

    public async Task RemoveEntity(Guid entityId)
    {
        var entity = await GetEntityById(entityId);
        RemoveEntity(entity);
    }

    public async Task SaveChanges()
    {
        await _context.SaveChangesAsync();
    }

    public void Dispose()
    {
        _context?.Dispose();
    }
}