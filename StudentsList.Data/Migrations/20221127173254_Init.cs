﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace StudentsList.Data.Migrations
{
    /// <inheritdoc />
    public partial class Init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Hobbies",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hobbies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Specialties",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Specialties", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FullName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Specialty = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Hobbies = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ClassTeacher = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LeadTeacher = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Teachers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TeacherType = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teachers", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Hobbies",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { new Guid("064def11-c3a7-4219-880b-c09af7cbe6ee"), false, "Yuzme" },
                    { new Guid("b65b631e-0727-4246-8290-1a16f071b5b5"), false, "Muzik" },
                    { new Guid("e2dce742-86c4-446b-b70a-1a2ea1794191"), false, "Edebiyat" }
                });

            migrationBuilder.InsertData(
                table: "Specialties",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { new Guid("2c98e1cd-a80e-48b3-ac82-b89e099a5242"), false, "Yazilim" },
                    { new Guid("5e5f0d16-710d-4e69-9ab0-8d00d5129c89"), false, "Felsefe" },
                    { new Guid("8436b2c6-2764-48c0-88d0-58e7b47ee69d"), false, "Matematik" }
                });

            migrationBuilder.InsertData(
                table: "Teachers",
                columns: new[] { "Id", "IsDeleted", "Name", "TeacherType" },
                values: new object[,]
                {
                    { new Guid("af16de7a-6d44-47fb-a0bc-ce835c09c994"), false, "Namik San", 0 },
                    { new Guid("b6c56a27-ebef-4f24-bb45-2a64cfe21036"), false, "Melda Sonmez", 0 },
                    { new Guid("cd0b521a-aeef-4e58-93a3-c428e3518aa7"), false, "Alp Yilmaz", 1 },
                    { new Guid("d11e87bf-c31a-4b70-ab7f-8a77588f79ee"), false, "Alp Yilmaz", 1 },
                    { new Guid("d80013f1-38af-4351-9542-b7f3bd25c08d"), false, "Nida Akyaz", 0 },
                    { new Guid("ec5c9cb2-7649-497a-8ca1-ee71a07c17e2"), false, "Mert Guneri", 1 }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Hobbies");

            migrationBuilder.DropTable(
                name: "Specialties");

            migrationBuilder.DropTable(
                name: "Students");

            migrationBuilder.DropTable(
                name: "Teachers");
        }
    }
}
