﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace StudentsList.Data.Migrations
{
    /// <inheritdoc />
    public partial class AddDeleteReasonToStudent : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Hobbies",
                keyColumn: "Id",
                keyValue: new Guid("064def11-c3a7-4219-880b-c09af7cbe6ee"));

            migrationBuilder.DeleteData(
                table: "Hobbies",
                keyColumn: "Id",
                keyValue: new Guid("b65b631e-0727-4246-8290-1a16f071b5b5"));

            migrationBuilder.DeleteData(
                table: "Hobbies",
                keyColumn: "Id",
                keyValue: new Guid("e2dce742-86c4-446b-b70a-1a2ea1794191"));

            migrationBuilder.DeleteData(
                table: "Specialties",
                keyColumn: "Id",
                keyValue: new Guid("2c98e1cd-a80e-48b3-ac82-b89e099a5242"));

            migrationBuilder.DeleteData(
                table: "Specialties",
                keyColumn: "Id",
                keyValue: new Guid("5e5f0d16-710d-4e69-9ab0-8d00d5129c89"));

            migrationBuilder.DeleteData(
                table: "Specialties",
                keyColumn: "Id",
                keyValue: new Guid("8436b2c6-2764-48c0-88d0-58e7b47ee69d"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("af16de7a-6d44-47fb-a0bc-ce835c09c994"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("b6c56a27-ebef-4f24-bb45-2a64cfe21036"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("cd0b521a-aeef-4e58-93a3-c428e3518aa7"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("d11e87bf-c31a-4b70-ab7f-8a77588f79ee"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("d80013f1-38af-4351-9542-b7f3bd25c08d"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("ec5c9cb2-7649-497a-8ca1-ee71a07c17e2"));

            migrationBuilder.AddColumn<string>(
                name: "DeleteReason",
                table: "Students",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.InsertData(
                table: "Hobbies",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { new Guid("45e2e551-64be-4b87-bea4-3da060234ab1"), false, "Yuzme" },
                    { new Guid("af8b3a47-60d2-49ee-963f-e44ec727fbce"), false, "Muzik" },
                    { new Guid("eae7b0a1-48ea-4723-811f-54397d284107"), false, "Edebiyat" }
                });

            migrationBuilder.InsertData(
                table: "Specialties",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { new Guid("589f7b67-3011-46e0-921c-8326e8f771cc"), false, "Matematik" },
                    { new Guid("c126f37f-3a64-46e5-a431-b3d566ba0f8a"), false, "Felsefe" },
                    { new Guid("e7a3384f-a5ee-4251-a5a2-1d05b43a808a"), false, "Yazilim" }
                });

            migrationBuilder.InsertData(
                table: "Teachers",
                columns: new[] { "Id", "IsDeleted", "Name", "TeacherType" },
                values: new object[,]
                {
                    { new Guid("0d0fc363-1cf3-4a27-895a-22a232552f5e"), false, "Mert Guneri", 1 },
                    { new Guid("4cb3071f-7c87-4196-b1d0-a94dfc93f0dd"), false, "Melda Sonmez", 0 },
                    { new Guid("7f2ff020-040a-4f3b-af75-94b71df04cd0"), false, "Alp Yilmaz", 1 },
                    { new Guid("8cc9e2d2-2621-41af-9eff-e753727b3c49"), false, "Mehmet Tuncay", 1 },
                    { new Guid("b69c482f-d3ed-4fdb-b7ec-78232cc1b8b6"), false, "Namik San", 0 },
                    { new Guid("d2d3ca56-5987-4475-a3d6-3fd4032229af"), false, "Nida Akyaz", 0 }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Hobbies",
                keyColumn: "Id",
                keyValue: new Guid("45e2e551-64be-4b87-bea4-3da060234ab1"));

            migrationBuilder.DeleteData(
                table: "Hobbies",
                keyColumn: "Id",
                keyValue: new Guid("af8b3a47-60d2-49ee-963f-e44ec727fbce"));

            migrationBuilder.DeleteData(
                table: "Hobbies",
                keyColumn: "Id",
                keyValue: new Guid("eae7b0a1-48ea-4723-811f-54397d284107"));

            migrationBuilder.DeleteData(
                table: "Specialties",
                keyColumn: "Id",
                keyValue: new Guid("589f7b67-3011-46e0-921c-8326e8f771cc"));

            migrationBuilder.DeleteData(
                table: "Specialties",
                keyColumn: "Id",
                keyValue: new Guid("c126f37f-3a64-46e5-a431-b3d566ba0f8a"));

            migrationBuilder.DeleteData(
                table: "Specialties",
                keyColumn: "Id",
                keyValue: new Guid("e7a3384f-a5ee-4251-a5a2-1d05b43a808a"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("0d0fc363-1cf3-4a27-895a-22a232552f5e"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("4cb3071f-7c87-4196-b1d0-a94dfc93f0dd"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("7f2ff020-040a-4f3b-af75-94b71df04cd0"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("8cc9e2d2-2621-41af-9eff-e753727b3c49"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("b69c482f-d3ed-4fdb-b7ec-78232cc1b8b6"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("d2d3ca56-5987-4475-a3d6-3fd4032229af"));

            migrationBuilder.DropColumn(
                name: "DeleteReason",
                table: "Students");

            migrationBuilder.InsertData(
                table: "Hobbies",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { new Guid("064def11-c3a7-4219-880b-c09af7cbe6ee"), false, "Yuzme" },
                    { new Guid("b65b631e-0727-4246-8290-1a16f071b5b5"), false, "Muzik" },
                    { new Guid("e2dce742-86c4-446b-b70a-1a2ea1794191"), false, "Edebiyat" }
                });

            migrationBuilder.InsertData(
                table: "Specialties",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { new Guid("2c98e1cd-a80e-48b3-ac82-b89e099a5242"), false, "Yazilim" },
                    { new Guid("5e5f0d16-710d-4e69-9ab0-8d00d5129c89"), false, "Felsefe" },
                    { new Guid("8436b2c6-2764-48c0-88d0-58e7b47ee69d"), false, "Matematik" }
                });

            migrationBuilder.InsertData(
                table: "Teachers",
                columns: new[] { "Id", "IsDeleted", "Name", "TeacherType" },
                values: new object[,]
                {
                    { new Guid("af16de7a-6d44-47fb-a0bc-ce835c09c994"), false, "Namik San", 0 },
                    { new Guid("b6c56a27-ebef-4f24-bb45-2a64cfe21036"), false, "Melda Sonmez", 0 },
                    { new Guid("cd0b521a-aeef-4e58-93a3-c428e3518aa7"), false, "Alp Yilmaz", 1 },
                    { new Guid("d11e87bf-c31a-4b70-ab7f-8a77588f79ee"), false, "Alp Yilmaz", 1 },
                    { new Guid("d80013f1-38af-4351-9542-b7f3bd25c08d"), false, "Nida Akyaz", 0 },
                    { new Guid("ec5c9cb2-7649-497a-8ca1-ee71a07c17e2"), false, "Mert Guneri", 1 }
                });
        }
    }
}
