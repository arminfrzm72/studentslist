﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace StudentsList.Data.Migrations
{
    /// <inheritdoc />
    public partial class RemoveHobbiesFromStudent : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Hobbies",
                keyColumn: "Id",
                keyValue: new Guid("5c25c6e1-974b-4307-87e1-5ab824a4319d"));

            migrationBuilder.DeleteData(
                table: "Hobbies",
                keyColumn: "Id",
                keyValue: new Guid("62ff7640-dec6-4903-9788-c087964fe35b"));

            migrationBuilder.DeleteData(
                table: "Hobbies",
                keyColumn: "Id",
                keyValue: new Guid("7f271277-c9b5-4b94-bdd4-ee975f903b90"));

            migrationBuilder.DeleteData(
                table: "Specialties",
                keyColumn: "Id",
                keyValue: new Guid("06d746e9-8e07-45b6-a40f-8f1ba9d879cc"));

            migrationBuilder.DeleteData(
                table: "Specialties",
                keyColumn: "Id",
                keyValue: new Guid("3e2e3bb8-1393-4abf-8b66-eb2c511f85c3"));

            migrationBuilder.DeleteData(
                table: "Specialties",
                keyColumn: "Id",
                keyValue: new Guid("7d7fc092-c0f4-447a-b75f-95e208f206e8"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("034d1f60-b79a-4de8-8c32-270f5410dfd1"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("6225c45a-037d-4fc6-8631-25ca5c1ede20"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("79853469-5def-41e0-a529-afa7393737ce"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("7a9527b0-b3a8-4974-aa4d-1f6c2fe16bd7"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("8502f725-2b09-418b-812f-740a5a2050f2"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("c79151ea-f8c5-4bdd-84cd-6712de78dffb"));

            migrationBuilder.DropColumn(
                name: "Hobbies",
                table: "Students");

            migrationBuilder.InsertData(
                table: "Hobbies",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { new Guid("2b0f57d4-4edc-43f4-818c-723a021b451c"), false, "Muzik" },
                    { new Guid("54620ea9-50c0-4770-ac87-509605bbe1bd"), false, "Yuzme" },
                    { new Guid("81d3f319-d145-44d4-a5b2-d84c33db7b4e"), false, "Edebiyat" }
                });

            migrationBuilder.InsertData(
                table: "Specialties",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { new Guid("722d006d-182c-4293-87c8-b2fc29b99c2b"), false, "Matematik" },
                    { new Guid("a63c5067-07f5-4adc-8aae-fa22f2d0a2c8"), false, "Felsefe" },
                    { new Guid("f190d1f1-e728-4905-9ae7-ec99412ed280"), false, "Yazilim" }
                });

            migrationBuilder.InsertData(
                table: "Teachers",
                columns: new[] { "Id", "IsDeleted", "Name", "TeacherType" },
                values: new object[,]
                {
                    { new Guid("4b9a9f94-00b4-4a79-b59a-d183b0f4432d"), false, "Mehmet Tuncay", 1 },
                    { new Guid("86cb82a0-5376-4633-a246-c00449507596"), false, "Mert Guneri", 1 },
                    { new Guid("89dabcf3-8538-45d2-915d-dc5eff327441"), false, "Melda Sonmez", 0 },
                    { new Guid("989a741c-8d1a-49f6-b6a5-515dbb086067"), false, "Namik San", 0 },
                    { new Guid("ef5fbcfd-b69f-4680-9e2c-ae31ed705724"), false, "Nida Akyaz", 0 },
                    { new Guid("f874b53a-c216-4748-a2a9-46ce9c9adf60"), false, "Alp Yilmaz", 1 }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Hobbies",
                keyColumn: "Id",
                keyValue: new Guid("2b0f57d4-4edc-43f4-818c-723a021b451c"));

            migrationBuilder.DeleteData(
                table: "Hobbies",
                keyColumn: "Id",
                keyValue: new Guid("54620ea9-50c0-4770-ac87-509605bbe1bd"));

            migrationBuilder.DeleteData(
                table: "Hobbies",
                keyColumn: "Id",
                keyValue: new Guid("81d3f319-d145-44d4-a5b2-d84c33db7b4e"));

            migrationBuilder.DeleteData(
                table: "Specialties",
                keyColumn: "Id",
                keyValue: new Guid("722d006d-182c-4293-87c8-b2fc29b99c2b"));

            migrationBuilder.DeleteData(
                table: "Specialties",
                keyColumn: "Id",
                keyValue: new Guid("a63c5067-07f5-4adc-8aae-fa22f2d0a2c8"));

            migrationBuilder.DeleteData(
                table: "Specialties",
                keyColumn: "Id",
                keyValue: new Guid("f190d1f1-e728-4905-9ae7-ec99412ed280"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("4b9a9f94-00b4-4a79-b59a-d183b0f4432d"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("86cb82a0-5376-4633-a246-c00449507596"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("89dabcf3-8538-45d2-915d-dc5eff327441"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("989a741c-8d1a-49f6-b6a5-515dbb086067"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("ef5fbcfd-b69f-4680-9e2c-ae31ed705724"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("f874b53a-c216-4748-a2a9-46ce9c9adf60"));

            migrationBuilder.AddColumn<string>(
                name: "Hobbies",
                table: "Students",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.InsertData(
                table: "Hobbies",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { new Guid("5c25c6e1-974b-4307-87e1-5ab824a4319d"), false, "Yuzme" },
                    { new Guid("62ff7640-dec6-4903-9788-c087964fe35b"), false, "Edebiyat" },
                    { new Guid("7f271277-c9b5-4b94-bdd4-ee975f903b90"), false, "Muzik" }
                });

            migrationBuilder.InsertData(
                table: "Specialties",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { new Guid("06d746e9-8e07-45b6-a40f-8f1ba9d879cc"), false, "Yazilim" },
                    { new Guid("3e2e3bb8-1393-4abf-8b66-eb2c511f85c3"), false, "Felsefe" },
                    { new Guid("7d7fc092-c0f4-447a-b75f-95e208f206e8"), false, "Matematik" }
                });

            migrationBuilder.InsertData(
                table: "Teachers",
                columns: new[] { "Id", "IsDeleted", "Name", "TeacherType" },
                values: new object[,]
                {
                    { new Guid("034d1f60-b79a-4de8-8c32-270f5410dfd1"), false, "Mert Guneri", 1 },
                    { new Guid("6225c45a-037d-4fc6-8631-25ca5c1ede20"), false, "Nida Akyaz", 0 },
                    { new Guid("79853469-5def-41e0-a529-afa7393737ce"), false, "Melda Sonmez", 0 },
                    { new Guid("7a9527b0-b3a8-4974-aa4d-1f6c2fe16bd7"), false, "Namik San", 0 },
                    { new Guid("8502f725-2b09-418b-812f-740a5a2050f2"), false, "Alp Yilmaz", 1 },
                    { new Guid("c79151ea-f8c5-4bdd-84cd-6712de78dffb"), false, "Mehmet Tuncay", 1 }
                });
        }
    }
}
