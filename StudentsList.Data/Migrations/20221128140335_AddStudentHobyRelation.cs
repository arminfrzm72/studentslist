﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace StudentsList.Data.Migrations
{
    /// <inheritdoc />
    public partial class AddStudentHobyRelation : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Hobbies",
                keyColumn: "Id",
                keyValue: new Guid("45e2e551-64be-4b87-bea4-3da060234ab1"));

            migrationBuilder.DeleteData(
                table: "Hobbies",
                keyColumn: "Id",
                keyValue: new Guid("af8b3a47-60d2-49ee-963f-e44ec727fbce"));

            migrationBuilder.DeleteData(
                table: "Hobbies",
                keyColumn: "Id",
                keyValue: new Guid("eae7b0a1-48ea-4723-811f-54397d284107"));

            migrationBuilder.DeleteData(
                table: "Specialties",
                keyColumn: "Id",
                keyValue: new Guid("589f7b67-3011-46e0-921c-8326e8f771cc"));

            migrationBuilder.DeleteData(
                table: "Specialties",
                keyColumn: "Id",
                keyValue: new Guid("c126f37f-3a64-46e5-a431-b3d566ba0f8a"));

            migrationBuilder.DeleteData(
                table: "Specialties",
                keyColumn: "Id",
                keyValue: new Guid("e7a3384f-a5ee-4251-a5a2-1d05b43a808a"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("0d0fc363-1cf3-4a27-895a-22a232552f5e"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("4cb3071f-7c87-4196-b1d0-a94dfc93f0dd"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("7f2ff020-040a-4f3b-af75-94b71df04cd0"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("8cc9e2d2-2621-41af-9eff-e753727b3c49"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("b69c482f-d3ed-4fdb-b7ec-78232cc1b8b6"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("d2d3ca56-5987-4475-a3d6-3fd4032229af"));

            migrationBuilder.CreateTable(
                name: "StudentHobbies",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    StudentId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    HobyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentHobbies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StudentHobbies_Hobbies_HobyId",
                        column: x => x.HobyId,
                        principalTable: "Hobbies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentHobbies_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Hobbies",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { new Guid("5c25c6e1-974b-4307-87e1-5ab824a4319d"), false, "Yuzme" },
                    { new Guid("62ff7640-dec6-4903-9788-c087964fe35b"), false, "Edebiyat" },
                    { new Guid("7f271277-c9b5-4b94-bdd4-ee975f903b90"), false, "Muzik" }
                });

            migrationBuilder.InsertData(
                table: "Specialties",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { new Guid("06d746e9-8e07-45b6-a40f-8f1ba9d879cc"), false, "Yazilim" },
                    { new Guid("3e2e3bb8-1393-4abf-8b66-eb2c511f85c3"), false, "Felsefe" },
                    { new Guid("7d7fc092-c0f4-447a-b75f-95e208f206e8"), false, "Matematik" }
                });

            migrationBuilder.InsertData(
                table: "Teachers",
                columns: new[] { "Id", "IsDeleted", "Name", "TeacherType" },
                values: new object[,]
                {
                    { new Guid("034d1f60-b79a-4de8-8c32-270f5410dfd1"), false, "Mert Guneri", 1 },
                    { new Guid("6225c45a-037d-4fc6-8631-25ca5c1ede20"), false, "Nida Akyaz", 0 },
                    { new Guid("79853469-5def-41e0-a529-afa7393737ce"), false, "Melda Sonmez", 0 },
                    { new Guid("7a9527b0-b3a8-4974-aa4d-1f6c2fe16bd7"), false, "Namik San", 0 },
                    { new Guid("8502f725-2b09-418b-812f-740a5a2050f2"), false, "Alp Yilmaz", 1 },
                    { new Guid("c79151ea-f8c5-4bdd-84cd-6712de78dffb"), false, "Mehmet Tuncay", 1 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_StudentHobbies_HobyId",
                table: "StudentHobbies",
                column: "HobyId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentHobbies_StudentId",
                table: "StudentHobbies",
                column: "StudentId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StudentHobbies");

            migrationBuilder.DeleteData(
                table: "Hobbies",
                keyColumn: "Id",
                keyValue: new Guid("5c25c6e1-974b-4307-87e1-5ab824a4319d"));

            migrationBuilder.DeleteData(
                table: "Hobbies",
                keyColumn: "Id",
                keyValue: new Guid("62ff7640-dec6-4903-9788-c087964fe35b"));

            migrationBuilder.DeleteData(
                table: "Hobbies",
                keyColumn: "Id",
                keyValue: new Guid("7f271277-c9b5-4b94-bdd4-ee975f903b90"));

            migrationBuilder.DeleteData(
                table: "Specialties",
                keyColumn: "Id",
                keyValue: new Guid("06d746e9-8e07-45b6-a40f-8f1ba9d879cc"));

            migrationBuilder.DeleteData(
                table: "Specialties",
                keyColumn: "Id",
                keyValue: new Guid("3e2e3bb8-1393-4abf-8b66-eb2c511f85c3"));

            migrationBuilder.DeleteData(
                table: "Specialties",
                keyColumn: "Id",
                keyValue: new Guid("7d7fc092-c0f4-447a-b75f-95e208f206e8"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("034d1f60-b79a-4de8-8c32-270f5410dfd1"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("6225c45a-037d-4fc6-8631-25ca5c1ede20"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("79853469-5def-41e0-a529-afa7393737ce"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("7a9527b0-b3a8-4974-aa4d-1f6c2fe16bd7"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("8502f725-2b09-418b-812f-740a5a2050f2"));

            migrationBuilder.DeleteData(
                table: "Teachers",
                keyColumn: "Id",
                keyValue: new Guid("c79151ea-f8c5-4bdd-84cd-6712de78dffb"));

            migrationBuilder.InsertData(
                table: "Hobbies",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { new Guid("45e2e551-64be-4b87-bea4-3da060234ab1"), false, "Yuzme" },
                    { new Guid("af8b3a47-60d2-49ee-963f-e44ec727fbce"), false, "Muzik" },
                    { new Guid("eae7b0a1-48ea-4723-811f-54397d284107"), false, "Edebiyat" }
                });

            migrationBuilder.InsertData(
                table: "Specialties",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { new Guid("589f7b67-3011-46e0-921c-8326e8f771cc"), false, "Matematik" },
                    { new Guid("c126f37f-3a64-46e5-a431-b3d566ba0f8a"), false, "Felsefe" },
                    { new Guid("e7a3384f-a5ee-4251-a5a2-1d05b43a808a"), false, "Yazilim" }
                });

            migrationBuilder.InsertData(
                table: "Teachers",
                columns: new[] { "Id", "IsDeleted", "Name", "TeacherType" },
                values: new object[,]
                {
                    { new Guid("0d0fc363-1cf3-4a27-895a-22a232552f5e"), false, "Mert Guneri", 1 },
                    { new Guid("4cb3071f-7c87-4196-b1d0-a94dfc93f0dd"), false, "Melda Sonmez", 0 },
                    { new Guid("7f2ff020-040a-4f3b-af75-94b71df04cd0"), false, "Alp Yilmaz", 1 },
                    { new Guid("8cc9e2d2-2621-41af-9eff-e753727b3c49"), false, "Mehmet Tuncay", 1 },
                    { new Guid("b69c482f-d3ed-4fdb-b7ec-78232cc1b8b6"), false, "Namik San", 0 },
                    { new Guid("d2d3ca56-5987-4475-a3d6-3fd4032229af"), false, "Nida Akyaz", 0 }
                });
        }
    }
}
