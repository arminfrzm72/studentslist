﻿using StudentsList.Data.Entities.Common;

namespace StudentsList.Data.Entities.Teacher
{
    public class Teacher:BaseEntity
    {
        public string? Name { get; set; }
        public TeacherType TeacherType { get; set; }
    }

    public enum TeacherType
    {
        LeadTeacher,
        ClassTeacher
    }
}
