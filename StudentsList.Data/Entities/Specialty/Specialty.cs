﻿using StudentsList.Data.Entities.Common;

namespace StudentsList.Data.Entities.Specialty
{
    public class Specialty:BaseEntity
    {
        public string Name { get; set; }
       
    }
}
