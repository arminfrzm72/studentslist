﻿using System.ComponentModel.DataAnnotations;

namespace StudentsList.Data.Entities.Common
{
    public class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }

    }
}
