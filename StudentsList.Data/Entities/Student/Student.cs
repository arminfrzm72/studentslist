﻿using StudentsList.Data.Entities.Common;
using StudentsList.Data.Entities.Hoby;

namespace StudentsList.Data.Entities.Student
{
    public class Student:BaseEntity
    {
        public string FullName { get; set; }
        public string Specialty { get; set; }
        public string ClassTeacher { get; set; }
        public string LeadTeacher { get; set; }
        public string DeleteReason { get; set; }

        #region Relation

        public ICollection<StudentHoby>? StudentHobbies { get; set; } = new List<StudentHoby>();

        #endregion
    }
}
