﻿using StudentsList.Data.Entities.Common;

namespace StudentsList.Data.Entities.Hoby
{
    public class Hoby : BaseEntity
    {
        public string Name { get; set; }

        #region Relation

        public ICollection<StudentHoby> StudentHobbies { get; set; }

        #endregion
    }
}

