﻿using StudentsList.Data.Entities.Common;

namespace StudentsList.Data.Entities.Hoby;

public class StudentHoby:BaseEntity
{
    public Guid StudentId { get; set; }
    public Guid HobyId { get; set; }

    #region Relation

    public Student.Student Student { get; set; }
    public Hoby Hoby { get; set; }

    #endregion
}