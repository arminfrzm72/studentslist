﻿using Microsoft.EntityFrameworkCore;
using StudentsList.Data.Entities.Hoby;
using StudentsList.Data.Entities.Specialty;
using StudentsList.Data.Entities.Student;
using StudentsList.Data.Entities.Teacher;

namespace StudentsList.Data.Context
{
    public class StudentsListContext : DbContext
    {
        public StudentsListContext(DbContextOptions<StudentsListContext> options) : base(options)
        {
        }

        #region Db Sets

        public DbSet<Student> Students { get; set; }
        public DbSet<Specialty> Specialties { get; set; }
        public DbSet<Hoby> Hobbies { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<StudentHoby> StudentHobbies { get; set; }


        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            var cascades = modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetForeignKeys())
                .Where(fk => fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascades)
            {
                fk.DeleteBehavior = DeleteBehavior.Restrict;
            }

            modelBuilder.Entity<Specialty>().HasData(
                new Specialty() { Id = Guid.NewGuid(), IsDeleted = false, Name = "Felsefe" },
                new Specialty() { Id = Guid.NewGuid(), IsDeleted = false, Name = "Matematik" },
                new Specialty() { Id = Guid.NewGuid(), IsDeleted = false, Name = "Yazilim" }
            );
            modelBuilder.Entity<Hoby>().HasData(
                new Hoby() { Id = Guid.NewGuid(), IsDeleted = false, Name = "Yuzme" },
                new Hoby() { Id = Guid.NewGuid(), IsDeleted = false, Name = "Edebiyat" },
                new Hoby() { Id = Guid.NewGuid(), IsDeleted = false, Name = "Muzik" }
            );
            modelBuilder.Entity<Teacher>().HasData(
                new Teacher() { Id = Guid.NewGuid(), IsDeleted = false, Name = "Alp Yilmaz", TeacherType = TeacherType.ClassTeacher },
                new Teacher() { Id = Guid.NewGuid(), IsDeleted = false, Name = "Mehmet Tuncay", TeacherType = TeacherType.ClassTeacher },
                new Teacher() { Id = Guid.NewGuid(), IsDeleted = false, Name = "Mert Guneri", TeacherType = TeacherType.ClassTeacher },
                new Teacher() { Id = Guid.NewGuid(), IsDeleted = false, Name = "Melda Sonmez", TeacherType = TeacherType.LeadTeacher },
                new Teacher() { Id = Guid.NewGuid(), IsDeleted = false, Name = "Namik San", TeacherType = TeacherType.LeadTeacher },
                new Teacher() { Id = Guid.NewGuid(), IsDeleted = false, Name = "Nida Akyaz", TeacherType = TeacherType.LeadTeacher }
            );

            modelBuilder.Entity<Student>().HasQueryFilter(p => !p.IsDeleted);
            modelBuilder.Entity<Specialty>().HasQueryFilter(p => !p.IsDeleted);
            modelBuilder.Entity<Hoby>().HasQueryFilter(p => !p.IsDeleted);
            modelBuilder.Entity<Teacher>().HasQueryFilter(p => !p.IsDeleted);
            modelBuilder.Entity<StudentHoby>().HasQueryFilter(p => !p.IsDeleted);

            base.OnModelCreating(modelBuilder);
        }
    }
}
