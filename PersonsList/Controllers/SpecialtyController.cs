﻿using Microsoft.AspNetCore.Mvc;
using StudentsList.Application.Services.Interfaces;
using StudentsList.Application.Utilities.Common;
using StudentsList.CustomAttributes;

namespace StudentsList.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [HandleException]
    public class SpecialtyController : ControllerBase
    {
        private readonly ISpecialtyService _specialtyService;
        public SpecialtyController(ISpecialtyService specialtyService)
        {
            _specialtyService = specialtyService;
        }

        [HttpGet("GetAllSpecialties")]
        public async Task<IActionResult> GetAllSpecialtiesAsync()
        {
            return JsonResponseStatus.Success(await _specialtyService.GetAllSpecialtiesAsync());
        }
    }
}
