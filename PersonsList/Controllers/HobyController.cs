﻿using Microsoft.AspNetCore.Mvc;
using StudentsList.Application.Services.Interfaces;
using StudentsList.Application.Utilities.Common;
using StudentsList.CustomAttributes;

namespace StudentsList.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [HandleException]
    public class HobyController : ControllerBase
    {
        private readonly IHobyService _hobyService;
        public HobyController(IHobyService hobyService)
        {
            _hobyService = hobyService;
        }

        [HttpGet("GetAllHobbies")]
        public async Task<IActionResult> GetAllHobbiesAsync()
        {
            return JsonResponseStatus.Success(await _hobyService.GetAllHobbiesAsync());
        }
    }
}
