﻿using Microsoft.AspNetCore.Mvc;
using StudentsList.Application.Services.Interfaces;
using StudentsList.Application.Utilities.Common;
using StudentsList.CustomAttributes;

namespace StudentsList.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [HandleException]
    public class TeacherController : ControllerBase
    {
        private readonly ITeacherService _teacherService;
        public TeacherController(ITeacherService teacherService)
        {
            _teacherService = teacherService;
        }

        [HttpGet("GetAllClassTeachers")]
        public async Task<IActionResult> GetAllClassTeachersAsync()
        {
            return JsonResponseStatus.Success(await _teacherService.GetAllClassTeachersAsync());
        }

        [HttpGet("GetAllLeadTeachers")]
        public async Task<IActionResult> GetAllLeadTeachersAsync()
        {
            return JsonResponseStatus.Success(await _teacherService.GetAllLeadTeachersAsync());
        }
    }
}
