﻿using Microsoft.AspNetCore.Mvc;
using StudentsList.Application.DTOs.Student;
using StudentsList.Application.Services.Interfaces;
using StudentsList.Application.Utilities.Common;
using StudentsList.CustomAttributes;

namespace StudentsList.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [HandleException]
    public class StudentController : ControllerBase
    {
        private readonly IStudentService _studentService;
        public StudentController(IStudentService studentService)
        {
            _studentService = studentService;
        }

        [HttpPost("FilterStudents")]
        public async Task<IActionResult> FilterStudents([FromBody] FilterStudentsDto filter)
        {
            var students = await _studentService.GetStudentsByFilterAsync(filter);
            await Task.Delay(500);
            return JsonResponseStatus.Success(students);
        }

        [HttpPost("CreateStudent")]
        public async Task<IActionResult> CreateStudent([FromBody]AddOrEditStudentDto studentDto)
        {
            await _studentService.AddStudent(studentDto);
            return JsonResponseStatus.Success();
        }

        [HttpPut("EditStudent")]
        public async Task<IActionResult> EditStudent([FromBody] AddOrEditStudentDto studentDto)
        {
            await _studentService.EditStudent(studentDto);
            return JsonResponseStatus.Success();
        }

        //genelde http delete kullanilir ama burda entity yi hard delete etmedigim icin http put la hal ettim.
        [HttpPut("DeleteStudent")]
        public async Task<IActionResult> DeleteStudent([FromBody]DeleteStudentDto deleteStudentDto)
        {
            await _studentService.DeleteStudent(deleteStudentDto);
            return JsonResponseStatus.Success();
        }
    }
}
