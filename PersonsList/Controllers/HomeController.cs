﻿using Microsoft.AspNetCore.Mvc;

namespace PersonsList.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet("/")]
        public IActionResult Index()
        {
            return View();
        }
    }
}
