﻿using System.ComponentModel.DataAnnotations;

namespace StudentsList.Application.DTOs.Student;

public class DeleteStudentDto
{
    [Required]
    public Guid StudentId { get; set; }

    [Required]
    public string DeleteReason { get; set; }
}