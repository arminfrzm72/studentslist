﻿using System.ComponentModel.DataAnnotations;

namespace StudentsList.Application.DTOs.Student;

public class AddOrEditStudentDto
{
    public Guid StudentId { get; set; }
    [Display(Name = "Ad Soyad")]
    [Required(ErrorMessage = "{0} girilmesi zorunludur")]
    [MaxLength(100, ErrorMessage = "{0} {1} karakterden fazla olamaz")]
    public string FullName { get; set; }

    [Display(Name = "Bolum")]
    [Required(ErrorMessage = "{0} girilmesi zorunludur")]
    public string Specialty { get; set; }

    [Display(Name = "Hobiler")]
    [Required(ErrorMessage = "{0} girilmesi zorunludur")]
    public string[] Hobbies { get; set; }

    [Display(Name = "Sinif Ogretmen")]
    [Required(ErrorMessage = "{0} girilmesi zorunludur")]
    public string ClassTeacher { get; set; }

    [Display(Name = "rehber Ogretmen")]
    [Required(ErrorMessage = "{0} girilmesi zorunludur")]
    public string LeadTeacher { get; set; }
}