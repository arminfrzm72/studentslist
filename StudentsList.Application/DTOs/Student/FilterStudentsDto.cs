﻿using StudentsList.Application.DTOs.Paging;

namespace StudentsList.Application.DTOs.Student;

public class FilterStudentsDto : BasePaging
{
    public string? SearchQuery { get; set; }
    public List<string>? Hobbies { get; set; }
    public string? ClassTeacher { get; set; }
    public List<StudentDto>? Students { get; set; }
    public FilterStudentsDto SetPaging(BasePaging paging)
    {
        PageNumber = paging.PageNumber;
        PageCount = paging.PageCount;
        StartPage = paging.StartPage;
        EndPage = paging.EndPage;
        PageSize = paging.PageSize;
        SkipSize = paging.SkipSize;
        ActivePage = paging.ActivePage;
        return this;
    }
    public FilterStudentsDto SetStudents(List<StudentDto> students)
    {
        Students = students;
        return this;
    }
}