﻿using AutoMapper;
using StudentsList.Application.Infrastructure.Mapping;

namespace StudentsList.Application.DTOs.Student;

public class StudentDto:IMapFrom<Data.Entities.Student.Student>
{
    public Guid StudentId { get; set; }
    public string FullName { get; set; }
    public string Specialty { get; set; }
    public List<HobyDto> Hobbies { get; set; }
    public string ClassTeacher { get; set; }
    public string LeadTeacher { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<Data.Entities.Student.Student, StudentDto>().ForMember(d=>d.StudentId,i=>i.MapFrom(s=>s.Id)).AfterMap((student, dto, context) =>
        {
            dto.Hobbies = student.StudentHobbies != null ?  student.StudentHobbies.Select(h => h.Hoby).Select(e => context.Mapper.Map<HobyDto>(e)).ToList() : new List<HobyDto>();
        });
    }
}