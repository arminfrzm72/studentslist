﻿using AutoMapper;
using StudentsList.Application.Infrastructure.Mapping;
using StudentsList.Data.Entities.Teacher;

namespace StudentsList.Application.DTOs;

public class TeacherDto:IMapFrom<Teacher>
{
    public string? Name { get; set; }
    public TeacherType TeacherType  { get; set; }
    public void Mapping(Profile profile)
    {
        profile.CreateMap<Teacher, TeacherDto>().ForMember(dest=>dest.TeacherType,i=>i.MapFrom(src=>src.TeacherType));
    }
}