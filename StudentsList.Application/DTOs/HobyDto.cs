﻿using AutoMapper;
using StudentsList.Application.Infrastructure.Mapping;
using StudentsList.Data.Entities.Hoby;

namespace StudentsList.Application.DTOs;

public class HobyDto:IMapFrom<Hoby>
{
    public Guid Id { get; set; }
    public string Name { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<Hoby, HobyDto>().ReverseMap();
    }
}