﻿namespace StudentsList.Application.DTOs;

public class SpecialtyDto
{
    public string Name { get; set; }
}