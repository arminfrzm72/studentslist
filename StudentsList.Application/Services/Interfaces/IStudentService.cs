﻿using StudentsList.Application.DTOs.Student;

namespace StudentsList.Application.Services.Interfaces;

public interface IStudentService
{
    Task<FilterStudentsDto> GetStudentsByFilterAsync(FilterStudentsDto filter);
    Task<StudentDto> GetStudentByIdAsync(Guid studentId);
    Task AddStudent(AddOrEditStudentDto studentDto);
    Task EditStudent(AddOrEditStudentDto studentDto);
    Task DeleteStudent(DeleteStudentDto deleteStudentDto);
}