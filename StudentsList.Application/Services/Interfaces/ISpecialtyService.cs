﻿using StudentsList.Application.DTOs;

namespace StudentsList.Application.Services.Interfaces;

public interface ISpecialtyService
{
    Task<List<SpecialtyDto>> GetAllSpecialtiesAsync();
}