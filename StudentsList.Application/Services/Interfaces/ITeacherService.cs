﻿using StudentsList.Application.DTOs;

namespace StudentsList.Application.Services.Interfaces;

public interface ITeacherService
{
    Task<List<TeacherDto>> GetAllClassTeachersAsync();
    Task<List<TeacherDto>> GetAllLeadTeachersAsync();
}