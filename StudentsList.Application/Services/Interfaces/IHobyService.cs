﻿using StudentsList.Application.DTOs;
using StudentsList.Data.Entities.Hoby;

namespace StudentsList.Application.Services.Interfaces;

public interface IHobyService
{
    Task<List<HobyDto>> GetAllHobbiesAsync();
}