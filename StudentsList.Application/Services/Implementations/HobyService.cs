﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using StudentsList.Application.DTOs;
using StudentsList.Application.Services.Interfaces;
using StudentsList.Data.Entities.Hoby;
using StudentsList.Data.Repository;

namespace StudentsList.Application.Services.Implementations;

public class HobyService : IHobyService
{
    private readonly IGenericRepository<Hoby> _hobyRepository;
    private readonly IMapper _mapper;
    public HobyService(IGenericRepository<Hoby> hobyRepository, IMapper mapper)
    {
        _hobyRepository = hobyRepository;
        _mapper = mapper;
    }

    public async Task<List<HobyDto>> GetAllHobbiesAsync()
    {
        return await _hobyRepository.GetEntitiesQuery().Select(h => _mapper.Map<HobyDto>(h)).ToListAsync();
    }

    #region dispose

    public void Dispose()
    {
        _hobyRepository?.Dispose();
    }

    #endregion
}