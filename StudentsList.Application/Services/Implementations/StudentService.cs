﻿using StudentsList.Application.DTOs.Paging;
using StudentsList.Application.DTOs.Student;
using StudentsList.Application.Services.Interfaces;
using StudentsList.Data.Entities.Student;
using StudentsList.Data.Repository;
using Microsoft.EntityFrameworkCore;
using StudentsList.Application.Utilities.Paging;
using StudentsList.Data.Entities.Hoby;
using System.Transactions;
using StudentsList.Application.DTOs;
using System.Linq;
using AutoMapper;

namespace StudentsList.Application.Services.Implementations;

public class StudentService : IStudentService
{
    private readonly IGenericRepository<Student> _studentRepository;
    private readonly IGenericRepository<StudentHoby> _studentHobyRepository;
    private readonly IMapper _mapper;
    public StudentService(IGenericRepository<Student> studentRepository, IGenericRepository<StudentHoby> studentHobyRepository, IMapper mapper)
    {
        _studentRepository = studentRepository;
        _studentHobyRepository = studentHobyRepository;
        _mapper = mapper;
    }

    public async Task<StudentDto> GetStudentByIdAsync(Guid studentId)
    {
        var student = await _studentRepository.GetEntityById(studentId);
        if (student == null) throw new Exception("Ogenci Bulunamadi");
        return new StudentDto()
        {
            FullName = student.FullName,
            StudentId = student.Id,
            Specialty = student.Specialty,
            LeadTeacher = student.LeadTeacher,
            ClassTeacher = student.ClassTeacher,
        };
    }

    public async Task<FilterStudentsDto> GetStudentsByFilterAsync(FilterStudentsDto filter)
    {
        var studentsQuery = _studentRepository.GetEntitiesQuery().Include(e=>e.StudentHobbies)!.ThenInclude(e=>e.Hoby).AsQueryable();

        if (!string.IsNullOrEmpty(filter.SearchQuery))
            studentsQuery = studentsQuery.Where(s => s.FullName.Contains(filter.SearchQuery));
        if (filter.Hobbies is { Count: > 0 })
        {
            var filterHobbies = new List<Guid>();
            filter.Hobbies.ForEach(e =>
            {
                filterHobbies.Add(Guid.Parse(e));
            });
            studentsQuery = studentsQuery.SelectMany(h => (h.StudentHobbies ?? new List<StudentHoby>()).Where(e => filterHobbies.Contains(e.HobyId)))
                .Select(s => s.Student);
        }

        if (!string.IsNullOrEmpty(filter.ClassTeacher))
        {
            studentsQuery = studentsQuery.Where(s => s.ClassTeacher.Contains(filter.ClassTeacher));
        }
        var count = (int)Math.Ceiling(studentsQuery.Count() / (double)filter.PageSize);

        var pager = Pager.Build(count, filter.PageNumber, filter.PageSize);
        var students = await studentsQuery.Paging(pager).Select(s=>_mapper.Map<StudentDto>(s)).ToListAsync();
        //var students = await studentsQuery.Paging(pager).Select(s => new StudentDto()
        //{
        //    StudentId = s.Id,
        //    FullName = s.FullName,
        //    Specialty = s.Specialty,
        //    Hobbies = s.StudentHobbies.Select(e => e.Hoby).Select(e => new HobyDto()
        //    {
        //        Name = e.Name,
        //        Id = e.Id
        //    }).ToList(),
        //    ClassTeacher = s.ClassTeacher,
        //    LeadTeacher = s.LeadTeacher
        //}).ToListAsync();

        return filter.SetStudents(students).SetPaging(pager);
    }


    public async Task AddStudent(AddOrEditStudentDto studentDto)
    {
        using var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
        var student = new Student()
        {
            Id = Guid.NewGuid(),
            FullName = studentDto.FullName,
            Specialty = studentDto.Specialty,
            ClassTeacher = studentDto.ClassTeacher,
            LeadTeacher = studentDto.LeadTeacher,
            DeleteReason = "",
            StudentHobbies = new List<StudentHoby>()
        };
        await _studentRepository.AddEntity(student);
        await _studentRepository.SaveChanges();
        foreach (var hoby in studentDto.Hobbies)
        {
            await _studentHobyRepository.AddEntity(new StudentHoby()
            {
                HobyId = Guid.Parse(hoby),
                StudentId = student.Id
            });
            await _studentHobyRepository.SaveChanges();
        }
        transactionScope.Complete();
    }

    public async Task EditStudent(AddOrEditStudentDto studentDto)
    {
        using var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
        var student = await _studentRepository.GetEntityById(studentDto.StudentId);
        if (student == null) throw new Exception("Ogenci Bulunamadi");
        student.FullName = studentDto.FullName;
        student.Specialty = studentDto.Specialty;
        student.ClassTeacher = studentDto.ClassTeacher;
        student.LeadTeacher = studentDto.LeadTeacher;
        student.StudentHobbies = new List<StudentHoby>();
        _studentRepository.UpdateEntity(student);
        await _studentRepository.SaveChanges();
        var studentHobbies =await _studentHobyRepository.GetEntitiesQuery().Where(sh => sh.StudentId == student.Id).ToListAsync();
        foreach (var studentHobby in studentHobbies)
        {
            studentHobby.IsDeleted = true;
            _studentHobyRepository.UpdateEntity(studentHobby);
            await _studentHobyRepository.SaveChanges();
        }
        foreach (var hoby in studentDto.Hobbies)
        {
            await _studentHobyRepository.AddEntity(new StudentHoby()
            {
                HobyId = Guid.Parse(hoby),
                StudentId = student.Id
            });
            await _studentHobyRepository.SaveChanges();
        }
        transactionScope.Complete();
    }

    public async Task DeleteStudent(DeleteStudentDto deleteStudentDto)
    {
        var student = await _studentRepository.GetEntityById(deleteStudentDto.StudentId);
        if (student == null) throw new Exception("Ogenci Bulunamadi");
        student.IsDeleted = true;
        student.DeleteReason = deleteStudentDto.DeleteReason;
        _studentRepository.UpdateEntity(student);
        await _studentRepository.SaveChanges();
    }

    #region dispose

    public void Dispose()
    {
        _studentRepository?.Dispose();
    }

    #endregion
}