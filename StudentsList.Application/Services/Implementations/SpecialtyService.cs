﻿using Microsoft.EntityFrameworkCore;
using StudentsList.Application.DTOs;
using StudentsList.Application.Services.Interfaces;
using StudentsList.Data.Entities.Specialty;
using StudentsList.Data.Repository;

namespace StudentsList.Application.Services.Implementations;

public class SpecialtyService : ISpecialtyService
{
    private readonly IGenericRepository<Specialty> _specialtyRepository;
    public SpecialtyService(IGenericRepository<Specialty> specialtyRepository)
    {
        _specialtyRepository = specialtyRepository;
    }

    public async Task<List<SpecialtyDto>> GetAllSpecialtiesAsync()
    {
        return await _specialtyRepository.GetEntitiesQuery().Select(s=>new SpecialtyDto()
        {
            Name = s.Name
        }).ToListAsync();
    }

    #region dispose

    public void Dispose()
    {
        _specialtyRepository?.Dispose();
    }

    #endregion
}