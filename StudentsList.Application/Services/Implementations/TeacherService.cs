﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using StudentsList.Application.DTOs;
using StudentsList.Application.Services.Interfaces;
using StudentsList.Data.Entities.Teacher;
using StudentsList.Data.Repository;
using TeacherType = StudentsList.Data.Entities.Teacher.TeacherType;

namespace StudentsList.Application.Services.Implementations;

public class TeacherService : ITeacherService
{
    private readonly IGenericRepository<Teacher> _teacherRepository;
    private readonly IMapper _mapper;
    public TeacherService(IGenericRepository<Teacher> specialtyRepository, IMapper mapper)
    {
        _teacherRepository = specialtyRepository;
        _mapper = mapper;
    }

    public async Task<List<TeacherDto>> GetAllTeachersAsync()
    {
        return await _teacherRepository.GetEntitiesQuery().Select(t => _mapper.Map<TeacherDto>(t)).ToListAsync();
    }

    public async Task<List<TeacherDto>> GetAllClassTeachersAsync()
    {
        return await _teacherRepository.GetEntitiesQuery().Where(t => t.TeacherType == TeacherType.ClassTeacher).Select(t => _mapper.Map<TeacherDto>(t)).ToListAsync();
    }

    public async Task<List<TeacherDto>> GetAllLeadTeachersAsync()
    {
        return await _teacherRepository.GetEntitiesQuery().Where(t => t.TeacherType == TeacherType.LeadTeacher).Select(t => _mapper.Map<TeacherDto>(t)).ToListAsync();
    }

    #region dispose

    public void Dispose()
    {
        _teacherRepository?.Dispose();
    }

    #endregion
}