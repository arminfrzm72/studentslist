﻿using AutoMapper;

namespace StudentsList.Application.Infrastructure.Mapping;

public interface IMapFrom<T>
{
    void Mapping(Profile profile);
}