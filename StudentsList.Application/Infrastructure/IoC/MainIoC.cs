﻿using Microsoft.Extensions.DependencyInjection;
using StudentsList.Application.Services.Implementations;
using StudentsList.Application.Services.Interfaces;

namespace StudentsList.Application.Infrastructure.IoC;

public static class MainIoC
{
    public static IServiceCollection UseApplication(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddAutoMapper(cfg =>
        {
            cfg.AddMaps("StudentsList.Application");
            cfg.AllowNullCollections = true;
            cfg.AddProfile<Mapping.Mapping>();
        });
        serviceCollection.AddScoped<IStudentService, StudentService>();
        serviceCollection.AddScoped<ISpecialtyService, SpecialtyService>();
        serviceCollection.AddScoped<IHobyService, HobyService>();
        serviceCollection.AddScoped<ITeacherService, TeacherService>();

        return serviceCollection;
    }
}